This uses a lyric database (https://www.kaggle.com/mousehead/songlyrics) to create random song lyrics, combining random verses of the songs that rhyme.

The main part are the two functions in lyrcgem.py, the first one taking a verse and returning the number of syllables and the last two phonemes (using cmudict), the second one uses a list of verses to randomly combine them into songs. It is possible to set a rhyme scheme.
The notebook prepares the database and gives some examples. To avoid endless computing time, either a random sample is taken, or the precomputed phonemedf.csv.