import cmudict
import random

cmu = cmudict.dict()
phones = cmudict.symbols()


# count syllables and find ending phoneme of a verse
def sylrhyme(verse):

    spchar = "!?'\",.:;()"
    vowels = "yaeiou"
    syl = 0
    rhyme = list()
    consonants = ["B", "CH", "D", "DH", "EL", "F", "G", "HH",
                  "JH", "K", "L", "M", "N", "NG", "P", "R",
                  "S", "SH", "T", "TH", "V", "W", "Z", "ZH"]

    # remove special characters and split verse into words
    for x in spchar:

        verse = verse.replace(x, " ")

    verse = verse.split()

    # count isolated vowels - not as precise as counting syllables via cmudict, but works for words not in the dict
    for word in verse:

        z = "z"

        for x in word.lower():

            if x in vowels and z not in vowels:

                syl += 1

            z = x

    # return nothing if there are no syllables or word not in cmudict
    if syl == 0 or cmu[verse[-1].lower()] == list():

        return syl, list()

    word = cmu[verse[-1].lower()][0]

    # check whether the word is in the cmu dictionary
    # and return syllable count and last phonemes up to vowel
    if not word == list():

        # just a placeholder
        rh = "G"

        while rh in consonants or len(word) + len(rhyme) < 2:

            rh = word[-1]
            word = word[:-1]

            for i in "012":
                rh = rh.replace(i, "")

            rhyme = [rh] + rhyme

            if word == list():
                return syl, rhyme

    return syl, rhyme


# Takes a list with elements of the form [song index, syllables, [last phonemes], verse text]
# and returns a random new song in given rhyme scheme
def randsong(vlist, scheme="1122 3344 5566"):

    lyrics = [[i, ""] for i in scheme]

    rhymedict = {i: list() for i in scheme.replace(" ", "")}

    for i in rhymedict:

        rhyme = random.choice(vlist)[1:3]

        while rhyme[0] == 0:

            rhyme = random.choice(vlist)[1:3]

        rhymedict[i] = [[x[0], x[3]] for x in vlist if x[2][-min(len(x[2]), len(rhyme[1])):] == rhyme[1][-min(len(x[2]), len(rhyme[1])):] and abs(x[1]/rhyme[0]-1) < .15]

    for j in range(len(lyrics)):

        if not lyrics[j][0] == " ":

            lyrics[j] = random.choice(rhymedict[lyrics[j][0]])

        else:

            lyrics[j] = ""

    return lyrics

